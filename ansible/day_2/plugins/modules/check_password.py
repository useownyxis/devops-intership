#!/usr/bin/python3

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: check_password

short_description: Checking password if it correct password and consists of (at least) 1 number [0-9] 1 special symbol ans length of password more than 7 symbols

version_added: "1.0.0"

options:
    password:
        description: Password for checking
        required: true
        type: str
author:
    - Dmitry Yakubik 
'''


RETURN = r'''
message:
    description: The output message. If password correct return 'Valid Password', other 'Not A Valid Password {str}'
    type: str
    returned: always
    sample: 'goodbye'
'''

from ansible.module_utils.basic import AnsibleModule
import re

def run_module():
    module_args = dict(
        password=str(type='str', required=True)
    )

    result = dict(
        changed=False,
        message=''
    )

    module = AnsibleModule(
        argument_spec=module_args
    )

    template = re.compile(r'^(?=.*[0-9].*)(?=.*[a-z].*)(?=.*[A-Z].*)[0-9a-zA-Z$%#^_-]{8,}$')

    if bool(template.match(module.params['password'])):
        result['message'] = 'Valid Password'
    else:
        #result['message'] = f'Not A Valid Password {module.params['password']}'
        module.fail_json(f'Not A Valid Password {module.params['password']}', **result)

    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
