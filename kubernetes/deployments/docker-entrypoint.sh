service php7.4-fpm start
sed 's,{{DB_NAME}},'"${DB_NAME}"',g' -i /var/www/wordpress/wp-config.php
sed 's,{{DB_USER}},'"${DB_USER}"',g' -i /var/www/wordpress/wp-config.php
sed 's,{{DB_PASSWORD}},'"${DB_PASSWORD}"',g' -i /var/www/wordpress/wp-config.php
sed 's,{{DB_HOST}},'"${DB_HOST}"',g' -i /var/www/wordpress/wp-config.php
nginx -g "daemon off;"