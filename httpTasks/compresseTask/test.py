
import requests
import zlib

BUFFER_SIZE = 16 + zlib.MAX_WBITS

def getCompressStreamByURL(url):
    compressResponse = requests.get(
        url,
        headers={'accept-encoding':'gzip'},
        stream=True,
    )
    return compressResponse.raw.stream()

def getDecompressStream(compressStream):
    decompresser = zlib.decompressobj(BUFFER_SIZE)
    for chunk in compressStream:
        yield decompresser.decompress(chunk)
        
def writeStreamToFile(path, stream):
    fd = open(path, "wb")
    for iter in stream:
        fd.write(iter)
    fd.close()

compressStream = getCompressStreamByURL('https://google.com')
decompressStream = getDecompressStream(compressStream)
writeStreamToFile('./output/output.html', decompressStream)