from app import db

class Image(db.Model):
    __tablename__ = 'images'

    id = db.Column(db.String(), primary_key=True)
    path = db.Column(db.String())

    def __init__(self, id, path=''):
        self.id = id
        self.path = path