import filesystem
import datetime
import log
from app import db
from models import Image


def save_image(image):
    image_id = filesystem.get_uuid()
    image_path = filesystem.get_image_path(image_id)
    filesystem.save_image(image, image_path)
    db.session.add(Image(image_id, image_path))
    db.session.commit()
    return {'id':image_id}

def get_image_by_id(id):
    image = Image.query.filter_by(id=id).first()
    return filesystem.get_image(image.path)

def get_images_metainfo():
    result = []
    images = Image.query.all()
    for image in images:
        result.append({'id':image.id})
    return result
    
def delete_image_by_id(id):
    image = Image.query.filter_by(id=id).first()
    db.session.delete(image)
    db.session.commit()
    filesystem.delete_image(image.path)
    return