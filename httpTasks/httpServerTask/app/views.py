import json                    
import base64
from flask import Flask, request, jsonify, abort, Response, json, make_response
import database
from app import app

@app.route('/images', methods=['POST'])
def add_image():
    app.logger.info('/images POST')
    image = request.get_data()
    try:
        meta_data = database.save_image(image)
    except IOError:
        abort(500)
    return jsonify(meta_data), 201

@app.route('/images', methods=['GET'])
def get_images():
    app.logger.info('/images GET')  
    metadata = database.get_images_metainfo()
    response = make_response(
        jsonify(
            metadata
        ),
        200,
    )
    response.headers['Content-Type'] = 'application/json'
    return response

@app.route('/images/<id>', methods=['GET'])
def get_image(id):
    app.logger.info(f'/images/{id} GET')
    try:
        image = database.get_image_by_id(id)
    except IOError:
        abort(404)
    response = make_response(
        image,
        200,
    )
    response.headers['Content-Type'] = 'image/png'
    return response

@app.route('/images/<id>', methods=['DELETE'])
def delete_image(id):
    app.logger.info(f'/images/{id} DELETE')
    try:
        database.delete_image_by_id(id)
    except FileNotFoundError:
        return make_response('', 204)
    return make_response('', 204)