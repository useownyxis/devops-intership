#!/bin/bash

postgres_status=`service --status-all | grep postgresql`

if [[ $postgres_status == "" ]]
then
    echo "postgres is not working"
    echo "staring postgresql"
    postgres_status=`service postgresql start`
    echo "$postgres_status"
    postgres_status=`service --status-all | grep postgresql`
    if [[ $postgres_status == "" ]]
    then
        echo "Cannot start postgresql"
        exit 1
    fi
fi

echo "postgres is working"

#export DB_URL="postgresql://postgres:nuzdarova@localhost/postgres"
#export APP_SETTINGS="config.DevelopmentConfig"
#export FLASK_APP=app.py
#export IMAGE_SAVE_PATH="./images"

echo "starting server"

flask run

exit 0