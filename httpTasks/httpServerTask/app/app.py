from flask import Flask
import filesystem
import log
import os
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy


log.log_config('./test.log')

app = Flask(__name__)

app.config.from_object(os.environ['APP_SETTINGS'])
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


db = SQLAlchemy(app)

import database
import views

if __name__ == '__main__':
    app.run()