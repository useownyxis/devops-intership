import logging

def log_info(message):
    logging.info(message)

def log_debug(message):
    logging.debug(message)

def log_warning(message):
    logging.warning(message)

def log_error(message):
    logging.error(message)

def log_config(file):
    log = logging.getLogger('werkzeug') 
    log.setLevel(logging.ERROR)
    logging.basicConfig(filename=file, level=logging.DEBUG)
